# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000000000000000
SAVEHIST=90000000000000000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/noah/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall


#costum call to .bashrc_aliases
if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi

source $HOME/.bash_aliases

#my default browser
export BROWSER=/bin/brave

# changeing default text editor to vim
export EDITOR=vim

#suggested python thingy
PYTHONPATH=$HOME/lib/python
EDITOR=vim

export PYTHONPATH EDITOR

#DT'S colorscript random color startup
#colorscript random

#startup for weather
#curl wttr.in -s | sed '7q'


#sites recomende
# Syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=cyan,underline
ZSH_HIGHLIGHT_STYLES[precommand]=fg=cyan,underline
ZSH_HIGHLIGHT_STYLES[arg0]=fg=cyan
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
ZSH_HIGHLIGHT_PATTERNS=('rm -rf *' 'fg=white,bold,bg=red')
#autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# zsh options
setopt notify
setopt correct
setopt auto_cd
setopt auto_list
# some nice formatting for you
#export PROMPT='%B%F{yellow}%~>%b%f '
# END zsh data
# put your code here

#this is to make zsh preform more like bash in that it does not show % at the end of unfinished printed lines
unsetopt prompt_cr prompt_sp


#this is to help doom
export PATH="$HOME/.emacs.d/bin:$PATH"~


#this is to tell whta lock i am using
export LOCK="betterlockscreen"


#exports zathura
export PATH="/home/noah/.local/bin:$PATH"


# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

#starship prompt
eval "$(starship init zsh)"


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -al'
alias la='ls -A'
alias l='ls'
